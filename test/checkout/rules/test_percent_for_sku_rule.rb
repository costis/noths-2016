require_relative '../../test_helper'

class TestPercentForSKURule < Minitest::Test

  def test_returns_a_percent_discount
    rule = Rules::PercentForSKURule.new('101', 0.30)
    product = create_card_holder

    discounts = rule.process([product])

    assert_equal 1, discounts.size
  end

  def test_returns_discounts_for_all_matched_items
    rule = Rules::PercentForSKURule.new('101', 0.30)
    products = (1..3).map { create_card_holder }

    discounts = rule.process(products)

    assert_equal 3, discounts.size
  end

  def test_creates_the_correct_percentage_discount
    rule = Rules::PercentForSKURule.new('101', 0.30)
    products = (1..3).map { create_card_holder }

    discounts = rule.process(products)

    discounts.each do |discount|
      assert_equal 0.30, discount.percentage
    end
  end
end
