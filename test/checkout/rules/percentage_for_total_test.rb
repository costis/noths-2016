require_relative '../../test_helper'

class TestPercentageForTotal < Minitest::Test
  def test_process_returns_a_percentage_discount
    rule = Rules::PercentageForTotalRule.new(100, 0.10)
    discounts = rule.process(110)
    assert discounts.first.is_a?(PercentageDiscount)
  end

  def test_process_returns_the_correct_amount
    rule = Rules::PercentageForTotalRule.new(100, 0.10)
    discounts = rule.process(110)
    assert discounts.first.is_a?(PercentageDiscount)

    result = discounts.first.call(110)
    assert_equal(-11, result)
  end
end
