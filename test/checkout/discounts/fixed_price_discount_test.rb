require_relative '../../test_helper'

class DiscountTest < MiniTest::Test

  def test_discount
    product = mock()
    discount = FixedPriceDiscount.new(product, 8.50)

    assert_equal product, discount.product
    assert_equal -1.50, discount.call(10)
  end
end
