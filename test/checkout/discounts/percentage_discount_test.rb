require_relative '../../test_helper'

class TestPercentageDiscount < Minitest::Test
  def test_process
    discount = PercentageDiscount.new(0.10)

    assert_equal -10, discount.call(100)
  end
end
