require_relative '../test_helper'

class CheckoutTest < MiniTest::Test
  def test_returns_original_price_when_no_discounts
    rule = Rules::FixedPriceForQuantityRule.new('101', 2, 8.50)
    co = Checkout.new([rule])

    co.scan(create_tshirt)

    assert_equal 19.95, co.total
  end

  def test_discount_matched_quantity_sets_fixed_price
    rule = Rules::FixedPriceForQuantityRule.new('101', 2, 8.50)
    co = Checkout.new([rule])

    co.scan(create_card_holder)
    co.scan(create_card_holder)

    assert_equal 17.00, co.total
  end

  def test_discount_way_over_quantity_sets_fixed_price
    products = (1..4).map { create_card_holder }

    rule = Rules::FixedPriceForQuantityRule.new('101', 2, 100)
    co = Checkout.new([rule])

    products.each { |p| co.scan(p) }

    assert_equal 400.00, co.total
  end

  def test_discount_under_quantity_doesnt_trigger_rule
    rule = Rules::FixedPriceForQuantityRule.new('101', 2, 8.50)
    co = Checkout.new([rule])

    co.scan(create_card_holder)

    assert_equal 9.25, co.total
  end

  def test_mixed_discounted_and_no_discounted
    products = (1..4).map { create_card_holder }
    products << create_product('102', 'Random product', 999)

    rule = Rules::FixedPriceForQuantityRule.new('101', 2, 100)
    co = Checkout.new([rule])

    products.each { |p| co.scan(p) }

    assert_equal 1399.00, co.total
  end

  def test_umatched_rules_dont_apply
    products = (1..4).map { create_card_holder }

    rule_a = Rules::FixedPriceForQuantityRule.new('101', 2, 100)
    rule_b = Rules::FixedPriceForQuantityRule.new('102', 2, 100)
    co = Checkout.new([rule_a, rule_b])

    products.each { |p| co.scan(p) }

    assert_equal 400.00, co.total
  end

  def test_mixed_rules
    products = (1..4).map { create_product('101', 'Travel Card Holder', 9.25) }
    products << (1..4).map { create_product('102', 'Personalized cufflinks', 45) }
    products.flatten!

    rules = [
        Rules::FixedPriceForQuantityRule.new('101', 2, 5),
        Rules::FixedPriceForQuantityRule.new('102', 2, 2)
    ]

    co = Checkout.new(rules)
    products.each { |p| co.scan(p) }

    assert_equal 28.00, co.total
  end

  def test_checkout_rules
    rule_a = Rules::FixedPriceForQuantityRule.new('101', 2, 8.5)
    rule_b = Rules::PercentageForTotalRule.new(60, 0.10)
    co = Checkout.new([rule_a, rule_b])

    co.scan(create_card_holder)
    co.scan(create_cufflinks)
    co.scan(create_card_holder)
    co.scan(create_tshirt)

    assert_equal 73.76, co.total
  end

  def test_30_percent_on_all_card_holders
    rule = Rules::PercentForSKURule.new('101', 0.30)

    co = Checkout.new([rule])
    co.scan(create_card_holder)
    co.scan(create_card_holder)
    co.scan(create_card_holder)

    assert_equal 19.42, co.total
  end

  def test_rule_chaining
    rule1 = Rules::PercentForSKURule.new('101', 0.30)
    rule2 = Rules::PercentForSKURule.new('101', 0.30)

    co = Checkout.new([rule1, rule2])
    co.scan(create_card_holder)

    assert_equal(4.53, co.total)
  end
end
