require 'minitest/autorun'
require 'minitest/reporters'
require 'mocha/mini_test'

Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

require File.join(File.dirname(File.expand_path(__FILE__)), '..', 'lib', 'checkout')

def create_product(sku, title, price)
  product = mock()
  product.stubs(:sku).returns(sku)
  product.stubs(:price).returns(price)
  product.stubs(:title).returns(title)
  product
end

def create_card_holder
  create_product('101', 'Travel Card Holder', 9.25)
end

def create_cufflinks
  create_product('102', 'Personalized cufflinks', 45.00)
end

def create_tshirt
  create_product('103', 'Kids T-shirt', 19.95)
end
