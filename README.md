# Pricing Engine
This is an implementation of a basic rules based pricing engine.

## Concepts
There are two types of pricing rules:

  * *ProductRule* is applied to individual products.
  * *CheckoutRule* is applied to the basket total.
  
Each pricing rule creates and configures *Discount* objects which will later be applied to products/basket total.
Currently two types of *Discount* objects have been implemented: *Fixed* and *Percentage*.

Discounts can be stacked so if one product matches two discounts the second discount will be applied on
the result of the first discount.

## Installation / Testing
This lib has been tested with ruby-2.3.1. Install dependencies with `bundle install` and execute the test suite
with `rake`.