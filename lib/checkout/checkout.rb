class Checkout
  attr_reader :products

  def initialize(rules)
    @rules = rules
    @products = []
  end

  def scan(product)
    @products << product
  end

  def total
    (products_total_with_discounts + checkout_discount_amount).round(2)
  end

  private
    def products_total_with_discounts
      products_total + products_discount_amount
    end

    def products_total
      products.collect(&:price).reduce(0, :+)
    end

    def products_discount_amount
      products_discounts = products.map { |p| apply_product_discounts_for(p) }
      products_discounts.reduce(0, :+)
    end

    def apply_product_discounts_for(product)
      discounts = discounts_for_product(product)

      return 0 if discounts.empty?

      res = discounts.inject(product.price) do |amount, discount|
        discount = discount.call(amount)
        amount +=  discount
      end

      res - product.price
    end

    def checkout_discount_amount
      discount_amounts = checkout_discounts.map { |discount| discount.call(products_total_with_discounts) }
      discount_amounts.reduce(0, :+)
    end

    def discounts_for_product(product)
      product_discounts.select { |d| d.product == product }
    end

    def product_rules
      @rules.select { |r| r.is_a? Rules::ProductRule }
    end

    def checkout_rules
      @rules.select { |r| r.is_a? Rules::CheckoutRule }
    end

    def product_discounts
      product_rules.map { |rule| rule.process(products) }.flatten
    end

    def checkout_discounts
      checkout_rules.map { |rule| rule.process(products_total_with_discounts) }.flatten
    end
end