require_relative 'product_rule'

module Rules
  class FixedPriceForQuantityRule < ProductRule
    def initialize(sku, quantity, price)
      @sku = sku
      @quantity = quantity
      @price = price
    end

    def process(products)
      matched_products = products.select { |p| p.sku == @sku }

      if matched_products.size >= 2
        return  matched_products.map { |p| FixedPriceDiscount.new(p, @price) }
      end

      []
    end
  end

end
