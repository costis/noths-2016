require_relative 'product_rule'

module Rules
  class PercentForSKURule < ProductRule
    def initialize(sku, percent)
      @sku = sku
      @percent = percent
    end

    def process(products)
      matched_products = products.select { |p| p.sku == @sku }

      matched_products.inject([]) do |acc, product|
        acc << PercentageDiscount.new(product, @percent)
      end
    end

  end
end