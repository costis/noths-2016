require_relative 'checkout_rule'

module Rules
  class PercentageForTotalRule < CheckoutRule
    def initialize(threshold, percentage)
      @threshold = threshold
      @percentage = percentage
    end

    def process(amount)
      if(amount > @threshold)
        # return -(amount * @percentage)
        return [PercentageDiscount.new(@percentage)]
      end

      []
    end
  end
end
