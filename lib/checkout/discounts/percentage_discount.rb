class PercentageDiscount
  attr_reader :percentage, :product

  def initialize(product = nil, percentage)
    @product = product
    @percentage = percentage
  end

  def call(amount)
    -(amount * percentage)
  end
end