# A Discount holds a reference to the item which belongs to
# and a method for applying the discount. This way we
# can lazy run the discounts and chain them.
class FixedPriceDiscount
  attr_reader :product

  def initialize(product, fixed_price)
    @product = product
    @fixed_price = fixed_price
  end

  def call(price)
    @fixed_price - price
  end
end
