require_relative 'checkout/checkout'

require_relative 'checkout/rules/fixed_price_for_quantity_rule'
require_relative 'checkout/rules/percentage_for_total_rule'
require_relative 'checkout/rules/percent_for_sku_rule'

require_relative 'checkout/discounts/fixed_price_discount'
require_relative 'checkout/discounts/percentage_discount'
